module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',

      black: '#2C353D',
      white: '#f6f9ff',

      orange: {
        100: '#fff2e6',
        200: '#ffe6cd',
        300: '#ffdab3',
        400: '#ffbe80',
        500: '#ffa862',
        600: '#ff914a',
        700: '#ff7a3b',
        800: '#ff6633',
        brand: '#ff6633',
        900: '#d43900',
      },
      blue: {
        100: '#edf0f2',
        200: '#cfe4fb',
        300: '#97c3f2',
        400: '#67a8ee',
        500: '#398ee9',
        600: '#0066cc',
        brand: '#0066CC',
        700: '#215B83',
        800: '#2A414D',
      },
      gray: {
        100: '#E6F0F5',
        200: '#D8E3E9',
        300: '#B4C1C9',
        400: '#7F8E98',
        500: '#505D68',
        600: '#353F49',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
      brown: {
        100: '#fdfbfa',
        200: '#F5EAE6',
        300: '#E9DCD8',
        400: '#C9B9B4',
        500: '#98857F',
        600: '#685650',
        700: '#493A35',
        800: '#3D302C',
      },
      red: {
        800: '#9b2c2c',
        error: '#9b2c2c',
      },
      green: {
        800: '#276749',
        success: '#276749',
      },
    },
    extend: {
      borderRadius: {
        xl: '1rem',
        '2xl': '2rem',
      },
      fontFamily: {
        heading: ['Rubik', 'Arial', 'sans-serif'],
        body: ['Montserrat', 'Arial', 'sans-serif'],
        navbar: ['Montserrat', 'Arial', 'sans-serif'],
        footer: ['Montserrat', 'Arial', 'sans-serif'],
        modal: ['Montserrat', 'Arial', 'sans-serif'],
      },
      height: {
        'screen-1/2': '50vh',
        'screen-1/3': '33.333333vh',
        'screen-2/3': '66.666667vh',
        'screen-1/4': '25vh',
        'screen-2/4': '50vh',
        'screen-3/4': '75vh',
        'screen-1/5': '20vh',
        'screen-2/5': '40vh',
        'screen-3/5': '60vh',
        'screen-4/5': '80vh',
        'screen-50': '50vh',
        'screen-60': '60vh',
        'screen-70': '70vh',
        'screen-80': '80vh',
        'screen-90': '90vh',
      },
      maxHeight: {
        '80': '20rem', // 320px
        '100': '25rem', // 400px
        '120': '30rem', // 480px
        '140': '35rem', // 560px
        '160': '40rem', // 640px
        '200': '50rem', // 800px
      },
      maxWidth: {
        limit: '88rem', // 1408px
        'screen-xxl': '90rem', // 1440px
      },
      minWidth: {
        '1/4': '25%',
        '1/3': '33.333333%',
        '1/2': '50%',
        '3/5': '60%',
        '2/3': '66.666667%',
        '3/4': '75%',
        xs: '4rem',
        sm: '6rem',
        md: '8rem',
        lg: '10rem',
        xl: '12rem',
        '2xl': '16rem',
        '3xl': '20rem',
        '4xl': '24rem',
        '5xl': '32rem',
        '6xl': '42rem',
        '7xl': '56rem',
        '8xl': '72rem',
      },
      spacing: {
        '2px': '2px',
        '72': '18rem', // 288px
        '80': '20rem', // 320px
        '100': '25rem', // 400px
        '120': '30rem', // 480px
        '140': '35rem', // 560px
        '160': '40rem', // 640px
        '200': '50rem', // 800px
      },
      strokeWidth: {
        '3/2': '1.5',
      },
      transitionDuration: {
        '50': '50ms',
      },
      transitionProperty: {
        filter: 'filter',
      },
    },
  },
  variants: {
    display: ['responsive', 'group-hover'],
    fontWeight: ['responsive', 'hover', 'focus', 'group-hover'],
    margin: ['responsive', 'group-hover'],
  },
  plugins: [require('@tailwindcss/custom-forms')],
};
