module.exports = {
  '*.{js,vue}': ['eslint --fix'],
  '*.{json,yml,yaml,html,md}': ['prettier --write'],
};
