module.exports = {
  parserOptions: {
    parser: 'babel-eslint',
  },
  plugins: ['vue', 'gridsome'],
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended',
    'plugin:gridsome/recommended',
    'plugin:prettier/recommended',
    'prettier/vue',
  ],
  rules: {
    'vue/no-v-html': 'off',
    'vue/static-class-names-order': 'off',
    'vue/component-tags-order': 'off',
    'vue/no-static-inline-styles': 'off',
  },
  env: {
    browser: true,
    es6: true,
    node: true,
  },
};
