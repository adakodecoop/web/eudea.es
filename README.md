# EUDEA's Website

[![Netlify Status](https://api.netlify.com/api/v1/badges/1fbf4a94-5124-4608-8d02-d3bda2623714/deploy-status)](https://app.netlify.com/sites/eudea-es/deploys)
[![This project is using Percy.io for visual regression testing.](https://percy.io/static/images/percy-badge.svg)](https://percy.io/Adakode/eudea.es)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

Brand new website of EUDEA.

## Contributors

Designed, developed and maintained by

<!-- prettier-ignore-start -->

| [<img alt="laklau" src="https://secure.gravatar.com/avatar/15c5fa5af66362d8b756c2458a06821c?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/web/eudea.es/graphs/master) | [<img alt="zuzudev" src="https://secure.gravatar.com/avatar/f925124b0eed1eeab8513016914b9150?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/web/eudea.es/graphs/master) |
| :---: | :---: |
| [Klaudia Alvarez](https://github.com/laklau) | [Carles Muiños](https://github.com/zuzust) |

<!-- prettier-ignore-end -->

## Contact

Email: hola[@]adakode[.]org  
Twitter: [@adakodecoop](https://twitter.com/adakodecoop)  
Facebook: [adakodecoop](https://www.facebook.com/adakodecoop)  
LinkedIn: [adakodecoop](https://www.linkedin.com/company/adakodecoop)

## License

The code of this website is &copy; 2019-present [Adakode](https://adakode.org) under the terms of the [MIT-Hippocratic License](https://firstdonoharm.dev/version/1/2/license.html).  
The contents of this website are &copy; 2019-present [EUDEA](https://eudea.es) under the terms of the [Creative Commons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/deed) license.
