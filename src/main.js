// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

// Fonts
import 'typeface-montserrat';
import 'typeface-rubik';

// Global Styles
import '~/assets/styles/index.css';

// Third-party Global Components
import LazyHydrate from 'vue-lazy-hydration';

// Global Base Components
import ButtonLink from '~/components/common/ButtonLink';
import ContentSection from '~/components/common/ContentSection';
import CoverSection from '~/components/common/CoverSection';
import CoverSplitSection from '~/components/common/CoverSplitSection';
import HyperLink from '~/components/common/HyperLink';
import ItemGrid from '~/components/common/ItemGrid';
import SvgIcon from '~/components/common/SvgIcon';

// Layouts
import PageLayout from '~/layouts/Page';

/* eslint-disable-next-line no-unused-vars */
export default function(Vue, { router, head, isClient }) {
  // Third-party Global Components
  Vue.component('LazyHydrate', LazyHydrate);

  // Global Base Components
  Vue.component('ButtonLink', ButtonLink);
  Vue.component('ContentSection', ContentSection);
  Vue.component('CoverSection', CoverSection);
  Vue.component('CoverSplitSection', CoverSplitSection);
  Vue.component('HyperLink', HyperLink);
  Vue.component('ItemGrid', ItemGrid);
  Vue.component('SvgIcon', SvgIcon);

  // Default Layout
  Vue.component('PageLayout', PageLayout);

  // Vue Plugins

  // Head Metadata
  head.htmlAttrs = {
    lang: 'es',
  };
}
