import { Machine, interpret } from 'xstate';

const submitMachine = Machine(
  {
    id: 'submit',
    initial: 'idle',
    states: {
      idle: {
        on: { SUBMIT: 'pending' },
      },
      pending: {
        invoke: {
          id: 'send-submission',
          src: 'sendSubmission',
          onDone: { target: 'success' },
          onError: { target: 'error', actions: 'logResponse' },
        },
      },
      success: {
        after: { 7000: 'idle' },
      },
      error: {
        after: { 5000: 'idle' },
      },
    },
  },
  {
    actions: {
      logResponse: (ctx, event) => console.log(event.data),
    },
    services: {
      sendSubmission: (ctx, event) => _doSubmit(event.endpoint, event.form),
    },
  }
);

const _doSubmit = async (endpoint, form) => {
  const { api } = await import('~/api/repositories/netlify');
  return await api[endpoint](form);
};

export default {
  name: 'FormContainer',
  props: {
    endpoint: { type: String, default: 'send' },
  },
  data: () => ({
    submitService: interpret(submitMachine),
    status: submitMachine.initialState.value,
  }),
  created() {
    this.submitService
      .onTransition(state => (this.status = state.value))
      .start();
  },
  methods: {
    send(form) {
      this.submitService.send('SUBMIT', { endpoint: this.endpoint, form });
    },
  },
  render() {
    return this.$scopedSlots.default({
      send: this.send,
      submission: this.status,
    });
  },
};
