const flatten = (obj, prefix = '', res = {}) => {
  return Object.entries(obj).reduce((r, [key, val]) => {
    const k = `${prefix}${key}`;

    if (typeof val === 'object') {
      flatten(val, `${k}.`, r);
    } else {
      res[k] = val;
    }

    return r;
  }, res);
};

export default {
  flatten,
};
