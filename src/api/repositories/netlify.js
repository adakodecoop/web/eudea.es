'use strict';

import fetch from 'node-fetch';

const baseURL = '/api';
const headers = {
  'Content-Type': 'application/json',
};

const _call = async (lambda, payload) => {
  const response = await fetch(`${baseURL}/${lambda}`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ payload }),
  });

  if (!response.ok) {
    console.log(`Error (${response.status}): ${response.statusText}`);
    throw new Error('Network response was not ok.');
  }

  return await response.json();
};

export const api = {
  contact: async payload => await _call('contact', payload),
};
