'use strict';

const _fakeit = async (endpoint, payload) => {
  console.log(`fakeit:${endpoint}`, payload);

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() >= 0.5) {
        resolve(payload);
      } else {
        reject(new Error('Network response was not ok.'));
      }
    }, 1500);
  });
};

export const api = {
  contact: async payload => await _fakeit('contact', payload),
};
