export default {
  inject: ['site'],
  metaInfo() {
    const { siteName, siteDomain, social } = this.site.settings;
    const { title, description } = this.$page.content.meta;
    const { coverImage } = this.cover;

    const twitter = social.twitter
      .trim()
      .split('/')
      .filter(i => i.length > 0)
      .reverse()[0];

    return {
      title,
      meta: [
        {
          key: 'description',
          name: 'description',
          content: description,
        },
        {
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          name: 'twitter:site',
          content: `@${twitter}`,
        },
        {
          name: 'twitter:creator',
          content: `@${twitter}`,
        },
        {
          name: 'twitter:title',
          content: `${siteName} - ${title}`,
        },
        {
          name: 'twitter:description',
          content: description,
        },
        {
          name: 'twitter:image',
          content: `https://${siteDomain}${coverImage.image.src}`,
        },
        {
          name: 'twitter:image:alt',
          content: coverImage.description,
        },
        {
          name: 'og:url',
          content: `https://${siteDomain}${this.$route.path}`,
        },
        {
          name: 'og:type',
          content: 'website',
        },
        {
          name: 'og:title',
          content: `${siteName} - ${title}`,
        },
        {
          name: 'og:image',
          content: `https://${siteDomain}${coverImage.image.src}`,
        },
        {
          name: 'og:image:alt',
          content: coverImage.description,
        },
        {
          name: 'og:description',
          content: description,
        },
        {
          name: 'og:site_name',
          content: siteName,
        },
        {
          name: 'og:locale',
          content: 'es_ES',
        },
      ],
    };
  },
};
