import ServicioCoverSection from './ServicioCoverSection.vue';
import ServicioCorePointsSection from './ServicioCorePointsSection.vue';
import ServicioFeaturesSection from './ServicioFeaturesSection.vue';
import ServicioImageGallerySection from './ServicioImageGallerySection.vue';

export default {
  ServicioCoverSection,
  ServicioCorePointsSection,
  ServicioFeaturesSection,
  ServicioImageGallerySection,
};
