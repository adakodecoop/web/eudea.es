import ServiciosCoverSection from './ServiciosCoverSection.vue';
import ServiciosProjectScopeSection from './ServiciosProjectScopeSection.vue';
import ServiciosTrustUsSection from './ServiciosTrustUsSection.vue';

export default {
  ServiciosCoverSection,
  ServiciosProjectScopeSection,
  ServiciosTrustUsSection,
};
