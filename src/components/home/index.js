import HomeHeroSection from './HomeHeroSection.vue';
import HomeIntroSection from './HomeIntroSection.vue';
import HomeServicesSection from './HomeServicesSection.vue';
import HomeChooseUsSection from './HomeChooseUsSection.vue';

export default {
  HomeHeroSection,
  HomeIntroSection,
  HomeServicesSection,
  HomeChooseUsSection,
};
