import SectorCoverSection from './SectorCoverSection.vue';
import SectorAreasSection from './SectorAreasSection.vue';
import SectorImageGallerySection from './SectorImageGallerySection.vue';

export default {
  SectorCoverSection,
  SectorAreasSection,
  SectorImageGallerySection,
};
