import ContactoCoverSection from './ContactoCoverSection.vue';
import ContactoFormSection from './ContactoFormSection.vue';

export default {
  ContactoCoverSection,
  ContactoFormSection,
};
