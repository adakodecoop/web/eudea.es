import QuienesSomosCoverSection from './QuienesSomosCoverSection.vue';
import QuienesSomosCorePointsSection from './QuienesSomosCorePointsSection.vue';
import QuienesSomosPrinciplesSection from './QuienesSomosPrinciplesSection.vue';

export default {
  QuienesSomosCoverSection,
  QuienesSomosCorePointsSection,
  QuienesSomosPrinciplesSection,
};
