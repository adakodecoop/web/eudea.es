// See: https://purgecss.com/configuration.html#options

module.exports = {
  // Specify the paths to all of the files that should be analyzed by PurgeCSS.
  content: ['./src/**/*.vue'],
  // Include any special characters you're using in this regular expression.
  // See: https://tailwindcss.com/docs/controlling-file-size/#understanding-the-regex
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
  // Remove any unused @font-face rules.
  fontFace: false,
  // Remove unused keyframes.
  keyframes: true,
  // Remove unused CSS variables.
  variables: true,
  // Whitelist auto generated classes for transitions, router links and images.
  // See: https://github.com/ky-is/vue-cli-plugin-tailwind
  whitelistPatterns: [
    /-(leave|enter|appear)(|-(to|from|active))$/,
    /^(?!(|.*?:)cursor-move).+-move$/,
    /^active(|--exact)$/,
    /^g-image(|--lazy|--loading|--loaded)$/,
  ],
};
