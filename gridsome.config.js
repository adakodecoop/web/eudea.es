// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const postcss_import = require('postcss-import');
const tailwindcss = require('tailwindcss');
const postcss_preset_env = require('postcss-preset-env');
const postcss_purgecss = require('@fullhuman/postcss-purgecss');

module.exports = {
  siteName: 'EUDEA',
  siteUrl: 'https://www.eudea.es',
  siteDescription:
    'Soluciones de ingeniería, automatización, electricidad, telecontrol y eficiencia energética',

  cacheBusting: false, // Disable cache busting only if deploying to Netlify

  configureWebpack: {
    node: {
      fs: 'empty',
    },
  },

  chainWebpack(config) {
    config.module.rules.delete('svg');
    config.module
      .rule('svg')
      .test(/\.svg$/)
      .use('vue')
      .loader('vue-loader')
      .end()
      .use('svg-to-vue-component')
      .loader('svg-to-vue-component/loader');
  },

  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          postcss_import,
          tailwindcss,
          postcss_preset_env({ stage: 1 }),
          ...(process.env.NODE_ENV === 'production' ? [postcss_purgecss] : []),
        ],
      },
    },
  },

  transformers: {
    remark: {
      externalLinksRel: ['nofollow', 'noopener'],
      slug: false,
    },
    yamlPlus: {
      images: {
        fieldName: 'image',
        mediaPath: '../../static',
      },
      markdown: {
        fieldName: 'text',
      },
    },
  },

  templates: {
    ServicioTemplateContent: [
      {
        path: node => `/servicios/${node.fileInfo.name}`,
        component: 'src/templates/Servicio.vue',
      },
    ],
    SectorTemplateContent: [
      {
        path: node => `/sectores/${node.fileInfo.name}`,
        component: 'src/templates/Sector.vue',
      },
    ],
  },

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'HomePageContent',
        baseDir: 'content',
        path: 'home/index.yml',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'ServiciosPageContent',
        baseDir: 'content',
        path: 'servicios/index.yml',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'ServicioTemplateContent',
        baseDir: 'content',
        path: 'servicios/detail/*.yml',
        yamlPlus: {
          images: {
            mediaPath: '../../../static',
          },
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'CooperacionPageContent',
        baseDir: 'content',
        path: 'cooperacion/index.yml',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'SectorTemplateContent',
        baseDir: 'content',
        path: 'sectores/detail/*.yml',
        yamlPlus: {
          images: {
            mediaPath: '../../../static',
          },
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'QuienesSomosPageContent',
        baseDir: 'content',
        path: 'quienes-somos/index.yml',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'ContactoPageContent',
        baseDir: 'content',
        path: 'contacto/index.yml',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'LegalPageContent',
        baseDir: 'content',
        path: 'legal/index.md',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'PrivacidadPageContent',
        baseDir: 'content',
        path: 'privacidad/index.md',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'NotFoundPageContent',
        baseDir: 'content',
        path: '404/index.yml',
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
    },
    {
      use: 'gridsome-plugin-robots-txt',
      options: {
        policy: [
          {
            userAgent: '*',
            disallow: '/admin/',
          },
        ],
      },
    },
    {
      use: 'gridsome-plugin-netlify-cms',
    },
  ],
};
