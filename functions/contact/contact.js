/*
Hippocratic License

Copyright (c) 2019-present Adakode

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

* No Harm: The software may not be used by anyone for systems or activities
  that actively and knowingly endanger, harm, or otherwise threaten the physical,
  mental, economic, or general well-being of other individuals or groups,
  in violation of the United Nations Universal Declaration of Human Rights
  (https://www.un.org/en/universal-declaration-human-rights/).

* Services: If the Software is used to provide a service to others, the licensee shall,
  as a condition of use, require those others not to use the service in any way
  that violates the No Harm clause above.

* Enforceability: If any portion or provision of this License shall to any extent
  be declared illegal or unenforceable by a court of competent jurisdiction,
  then the remainder of this License, or the application of such portion or provision
  in circumstances other than those as to which it is so declared illegal or unenforceable,
  shall not be affected thereby, and each portion and provision of this Agreement
  shall be valid and enforceable to the fullest extent permitted by law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This Hippocratic License is an Ethical Source license (https://ethicalsource.dev)
derived from the MIT License, amended to limit the impact of the unethical use
of open source software.
*/
'use strict';

const Validator = require('validatorjs');
const { EmailProvider } = require('./email/providers');
const { httpErrors } = require('./utils/enums');

/**
 *
 * @param {object} error
 */
const buildErrorResponse = error => {
  console.log(error); // output to netlify function log

  if (error.message === httpErrors.METHOD_NOT_ALLOWED) {
    return { statusCode: 405, body: 'Method Not Allowed' };
  }

  if (error.message === httpErrors.UNPROCESSABLE_ENTITY) {
    return { statusCode: 422, body: 'Unprocessable Entity' };
  }
  return {
    statusCode: 500,
    body: 'Internal Server Error',
  };
};

/**
 *
 * @param {object} event
 */
const validateRequest = ({ httpMethod }) => {
  if (httpMethod !== 'POST') {
    throw new Error(httpErrors.METHOD_NOT_ALLOWED);
  }

  return true;
};

/**
 *
 * @param {object} event
 */
const manageSubmission = async ({ body }) => {
  const { payload: data } = JSON.parse(body);
  validateSubmission(data);

  return await sendSubmission(data);
};

/**
 *
 * @param {object} data
 */
const validateSubmission = data => {
  const rules = {
    name: 'required|regex:/^[a-zÀ-ÿ\\s]+$/i',
    email: 'required|email',
    phone: 'regex:/^\\d[\\d\\s]+\\d$/',
    subject: 'required',
    message: 'required',
    terms: 'accepted',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new Error(httpErrors.UNPROCESSABLE_ENTITY);
  }

  return true;
};

/**
 *
 * @param {object} data
 */
const sendSubmission = async data => {
  const emailService = EmailProvider.get('smtp');

  return await emailService.send('contact', data);
};

/**
 *
 * @example
 * netlify functions:invoke contact --no-identity --payload '{"payload": {"name": "Jane Doe", "email": "jane@doe.dev", "phone": null, "subject": "subject", "message": "message", "terms": true}}'
 *
 * @param {object} event
 * @param {object} ctx
 */
// eslint-disable-next-line no-unused-vars
exports.handler = async (event, ctx) => {
  try {
    validateRequest(event);

    const response = await manageSubmission(event);

    return {
      statusCode: 200,
      body: JSON.stringify(response),
    };
  } catch (error) {
    return buildErrorResponse(error);
  }
};
