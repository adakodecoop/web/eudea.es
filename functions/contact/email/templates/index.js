'use strict';

const contact = require('./contact');

const templates = {
  contact,
};

module.exports = {
  build: (template, data) => ({
    text: templates[template].text(data),
    html: templates[template].html(data),
  }),
};
