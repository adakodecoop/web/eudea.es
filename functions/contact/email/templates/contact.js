'use strict';

const mjml = require('mjml');

module.exports = {
  text: data => `
    Alguien ha contactado a través del formulario de contacto de la web. Este es su mensaje:

    Nombre: ${data.name}
    Correo: ${data.email}
    Teléfono: ${data.phone || ''}
    Asunto: ${data.subject}
    -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

    ${data.message}
  `,
  html: data => {
    const template = `
    <mjml>
      <mj-head>
        <mj-title>Nueva consulta via web</mj-title>
        <mj-font name="Roboto" href="https://fonts.googleapis.com/css?family=Roboto:300,500" />
        <mj-attributes>
          <mj-all font-family="Roboto, Helvetica, sans-serif" />
          <mj-text font-weight="300" font-size="16px" color="#2c353d" line-height="20px" />
          <mj-section padding="0px" />
        </mj-attributes>
      </mj-head>
      <mj-body>
        <mj-section padding-top="30px">
          <mj-column width="40%">
            <mj-image src="https://www.eudea.es/media/primary-logo.png" />
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column width="100%">
            <mj-text align="center" color="#685650" >
              <h1 style="font-size: 20px; font-weight: bold; color: #685650;">NUEVA CONSULTA VIA WEB</h1>
            </mj-text>
            <mj-divider width="50%" border-width="2px" border-color="#ff6633" border-style="dashed" />
          </mj-column>
        </mj-section>
        <mj-section padding-top="15px">
          <mj-column width="100%">
            <mj-text color="#685650">
              Alguien ha contactado a través del formulario de contacto de la web. Este es su mensaje:
            </mj-text>
          </mj-column>
        </mj-section>
        <mj-section padding-top="30px">
          <mj-column>
            <mj-text>
              <h2 style="font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0; color: #0066cc">Nombre:</h2>
              <p style="margin-top: 10; margin-bottom: 0">${data.name}</p>
            </mj-text>
            <mj-divider border-width="1px" border-color="#97c3f2" />
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text>
              <h2 style="font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0; color: #0066cc">Correo electrónico:</h2>
              <p style="margin-top: 10; margin-bottom: 0">${data.email}</p>
            </mj-text>
            <mj-divider border-width="1px" border-color="#97c3f2" />
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text>
              <h2 style="font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0; color: #0066cc">Teléfono:</h2>
              <p style="margin-top: 10; margin-bottom: 0">
                ${data.phone || ''}
              </p>
            </mj-text>
            <mj-divider border-width="1px" border-color="#97c3f2" />
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text>
              <h2 style="font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0; color: #0066cc">Asunto:</h2>
              <p style="margin-top: 10; margin-bottom: 0">${data.subject}</p>
            </mj-text>
            <mj-divider border-width="1px" border-color="#97c3f2" />
          </mj-column>
        </mj-section>
        <mj-section>
          <mj-column>
            <mj-text>
              <h2 style="font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0; color: #0066cc; line-height: 16px">Mensaje:</h2>
              <p style="line-height: 22px">${data.message}</p>
            </mj-text>
          </mj-column>
        </mj-section>
        <mj-section padding-top="15px" padding-bottom="10px">
          <mj-column>
            <mj-divider width="50%" border-width="2px" border-color="#ff6633" border-style="dashed" />
          </mj-column>
        </mj-section>
      </mj-body>
    </mjml>
    `;

    return mjml(template, { minify: true }).html;
  },
};
