'use strict';

const smtp = require('./smtp');

const providers = {
  smtp,
};

exports.EmailProvider = {
  get: name => providers[name],
};
