'use strict';

const nodemailer = require('nodemailer');
const templateFactory = require('../templates');

const _buildMessage = (template, data) => {
  const { text, html } = templateFactory.build(template, data);

  return {
    from: process.env.EMAIL_SENDER,
    to: process.env.EMAIL_RECIPIENT,
    subject: 'Nueva consulta via web',
    replyTo: data.email,
    text,
    html,
  };
};

const _getTransporter = () => {
  return nodemailer.createTransport({
    host: process.env.EMAIL_SMTP_HOST,
    port: process.env.EMAIL_SMTP_PORT,
    auth: {
      user: process.env.EMAIL_AUTH_USER,
      pass: process.env.EMAIL_AUTH_PASSWORD,
    },
  });
};

const _sendMail = async message => {
  // create reusable transporter object using the default SMTP transport
  const transporter = _getTransporter();

  // send mail with defined transport object
  return await transporter.sendMail(message);
};

module.exports = {
  send: async (template, data) => {
    const message = _buildMessage(template, data);
    return await _sendMail(message);
  },
};
