'use strict';

const httpErrors = {
  UNAUTHORIZED: '401',
  FORBIDDEN: '403',
  METHOD_NOT_ALLOWED: '405',
  UNPROCESSABLE_ENTITY: '422',
  INTERNAL_SERVER_ERROR: '500',
};

module.exports = { httpErrors };
