---
meta:
  title: Legal
  description: Información legal del website de EUDEA
---

# Aviso legal

## Propiedad Intelectual

EUDEA MercaVía, S.L. con C.I.F B-61.915.690 y domicilio en la Avenida Aragón 4, 44600 - Alcañiz (Teruel), constituida ante el Notario de Barcelona, D. Marco Antonio Alonso Hevia, e inscrita en el Registro Mercantil de Barcelona en fecha 21 Junio de 1999, Tomo 31717, Folio 91, Hoja B-199457, Inscripción 1 (en adelante EUDEA®).
EUDEA ofrece toda la información relacionada con el tipo de actividad que desarrolla, así como los productos y servicios que presta y realiza.

Esta web con todos los subdominios y directorios incluidos en la misma (en adelante y conjuntamente denominados como Portal), así como los servicios o contenidos que a través de ella se pueden obtener, están sujetos a los términos que se detallan en este Aviso Legal.

En cumplimiento de lo previsto en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, se informa que la totalidad del presente Portal es propiedad intelectual de EUDEA o en su defecto de la empresa Colaboradora que aporta la Documentación particular, no pudiendo ser objeto de explotación, reproducción, distribución, modificación, comunicación pública, cesión o transformación o cualquier otra forma de difusión no autorizada expresamente. Salvo en la medida que sea necesario para visualizarlo on-line. Sin perjuicio de lo anterior, se podrán imprimir páginas completas del Portal que se requieran para uso personal.

## Precisión de la Información

La información de este Portal está dirigida a clientes de EUDEA en España y no será de aplicación en el ámbito de otros países. EUDEA procurará que los contenidos de este Portal sean rigurosos y estén actualizados, pero deberán ser considerados por el usuario a modo de orientación e información, y sin que pueda estimarse como elemento determinante para la toma de decisiones o soluciones, declinando EUDEA todo tipo de responsabilidad por el uso que pueda verificarse de la misma en tal sentido, y de forma específica se ha de entender que dicha información no tendrá ninguna responsabilidad en caso de reclamaciones o pérdidas surgidas para terceros en relación con los contenidos del Portal.

EUDEA se reserva el derecho de actualizar, modificar o eliminar la información contenida en este Portal, pudiendo incluso limitar o no permitir el acceso a dicha información, sin previo aviso. Especialmente, EUDEA se reserva el derecho a eliminar, limitar o impedir el acceso a su Portal cuando surjan dificultades técnicas por hechos o circunstancias ajenos a EUDEA que, a su criterio, disminuyan o anulen los niveles de seguridad estándares adoptados para el adecuado funcionamiento de dicho Portal.
EUDEA en ningún caso será responsable de las pérdidas, daños o perjuicios de cualquier tipo que surjan por acceder y usar el Portal, incluyéndose, pero no limitándose, a los producidos en los sistemas informáticos o los provocados por la introducción de virus y/o ataques informáticos de los que este Portal es completamente ajeno.

EUDEA tampoco será responsable de los daños que pudieran sufrir los usuarios por un uso inadecuado de este Portal y, en modo alguno, de las caídas, interrupciones, ausencia o defecto de las comunicaciones.
EUDEA no responde de la veracidad, integridad o actualización de las informaciones en las que se indique otra fuente externa, así como tampoco de las contenidas en otras websites mediante hiperenlace o vínculo desde www.eudea.es, facilitados al usuario como alternativas de información, que se regirán por los términos y condiciones de utilización que a tal efecto resulten exigibles por los titulares de dichos websites, por lo que EUDEA no asume responsabilidad alguna en cuanto a hipotéticos perjuicios que pudieran originarse por el uso de las citadas informaciones. En ningún caso, los mencionados hiperenlaces serán considerados como recomendación, patrocinio, o distribución por parte de EUDEA de la información, productos y/o servicios, o, en general, contenidos de titularidad de terceros, ofrecidos por éstos o en cualquier forma por los mismos divulgados.

La información proporcionada a través de este Portal, mediante respuesta a consultas o reclamaciones, o la derivada de cualquier base de datos, tiene carácter meramente orientativo y en ningún caso podrá ser determinante para su aplicación.

EUDEA no se responsabiliza de las posibles discrepancias que, con carácter transitorio, puedan surgir entre la versión de sus documentos impresos y la versión electrónica de los mismos publicados en las páginas de su Portal.

## Jurisdicción Aplicable

Este Portal ha sido creado y funciona de acuerdo con la legislación española. Para la resolución de cualquier controversia en relación con este Portal serán aplicables exclusivamente las leyes españolas.
