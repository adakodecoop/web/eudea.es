---
meta:
  title: Privacidad
  description: Política de privacidad del website de EUDEA
---

# Política de privacidad

## Protección de Datos

EUDEA procesa sus datos personales de Registro con el debido cuidado y respeta la privacidad de cada persona que visite el Portal. Esta cláusula resume la información que EUDEA puede recopilar de la red y la forma con la que podrá ser utilizada.

### Datos Personales

En cumplimiento de lo dispuesto en la Ley Orgánica 15/1999, de Protección de datos de Carácter Personal, le informamos de que sus datos serán incorporados a un fichero automatizado del que es responsable EUDEA MercaVía,SL, en donde puede ejercer los derechos de acceso, rectificación, cancelación y oposición establecidos en la legislación aplicable, mediante remisión de solicitud escrita a la dirección comercial@eudea.es

EUDEA MercaVía,SL le garantiza que ha adoptado las medidas de seguridad en sus instalaciones, sistemas y ficheros que la legislación vigente exige en relación con la conservación y custodia de los datos, que serán utilizados conforme a las obligaciones de confidencialidad previstas en la Norma.

El usuario que registre sus datos en este Portal de EUDEA MercaVía,SL (www.eudea.es) autoriza a la misma a almacenar y utilizar sus datos de carácter personal, conforme a lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, con la finalidad de que le sea proporcionada información sobre los productos o servicios a que se refiera su solicitud, así como otros Servicios que pudiera ofrecer EUDEA a sus clientes. Asimismo, el usuario registrado NO autoriza la cesión de sus datos a terceros con fines publicitarios o de cualquier otra índole.

### Información identificable no personal recopilada automáticamente

A través del Portal sólo se obtiene, de manera automática, la información técnica mínima e imprescindible para prestar un servicio y navegación adecuados. En algunos casos, podemos recopilar información sobre Vd. de forma anónima. Ejemplos de este tipo de información incluyen el tipo de buscador de Internet que Vd. está utilizando, el tipo de sistema operativo y el nombre de dominio del la website desde el que se conectó a nuestro portal o anuncio.

## A quién contactar

Si Vd. ha presentado datos personales a EUDEA y le gustaría que la información fuera corregida o borrada de sus archivos, o en caso de que Vd. quiera saber qué datos personales procesamos sobre Vd., puede ponerse en contacto con EUDEA a la siguiente dirección electrónica: comercial@eudea.es

Nosotros haremos lo posible por atender su solicitud a la mayor brevedad posible.
