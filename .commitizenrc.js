// This is cz-customizable's .cz-config file

// prettier-ignore
module.exports = {
  allowCustomScopes: true,
  allowBreakingChanges: [
    'feat',
    'fix',
    'improvement',
  ],
  footerPrefix: 'Closes',
  // scopes: [
  //   'blog',
  //   'portfolio'
  // ],
  scopeOverrides: {
    test: [
      { name: 'unit' },
      { name: 'snapshot' },
      { name: 'e2e' },
      { name: 'visual' },
    ]
  },
  skipQuestions: [
    'footer'
  ],
  types: [
    { value: 'feat', name: 'feat: Adds a new feature' },
    { value: 'improvement', name: 'improvement: Improves a current implementation without adding a new feature or fixing a bug' },
    { value: 'fix', name: 'fix: Solves a bug' },
    { value: 'chore', name: "chore: Other changes that don't modify src or test files" },
    { value: 'docs', name: 'docs: Adds or alters documentation' },
    { value: 'test', name: 'test: Adds or modifies tests' },
    { value: 'refactor', name: 'refactor: Rewrites code without feature, performance or bug changes' },
    { value: 'build', name: 'build: Affects the build system or external dependencies' },
    { value: 'ci', name: 'ci: Changes CI configuration files and scripts' },
    { value: 'perf', name: 'perf: Improves performance' },
    { value: 'revert', name: 'revert: Reverts a previous commit' },
    { value: 'style', name: 'style: Improves formatting, white-space, identation, etc (not affecting the meaning of the code)' },
  ],
};
